import React from 'react';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDeleteLeft } from '@fortawesome/free-solid-svg-icons';

import style from './index.module.scss';

const ENTER_ID = 'enter';
const DELETE_ID = 'delete';
const KEY_LAYOUT = [
  ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'],
  ['', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ''],
  [ENTER_ID, 'z', 'x', 'c', 'v', 'b', 'n', 'm', DELETE_ID]
];

const Keyboard = () => {

  function getKeyLabel(key) {
    if (key === ENTER_ID) return 'Enter';
    if (key === DELETE_ID) return <FontAwesomeIcon icon={faDeleteLeft} />;
    return key;
  }

  function isSpacer(key) {
    return key === '';
  }

  function isActionKey(key) {
    return (
      key === ENTER_ID ||
      key === DELETE_ID
    );
  }

  return (
    <div className={style.wrap}>

      {KEY_LAYOUT.map((row, index) =>
        <div
          key={`keyboard-row-${index}`}
          className={style.row}>

          {row.map((key) => {

            if (isSpacer(key)) {
              return (
                <div className={style.spacer} />
              );
            }

            return (
              <button
                key={`keyboard-letter-${key}`}
                className={classNames({
                  [style.spacer]: isSpacer(key),
                  [style.actionKey]: isActionKey(key)
                })}>
                {getKeyLabel(key)}
              </button>
            );

          })}

        </div>
      )}

    </div>
  );
}

export default Keyboard;
