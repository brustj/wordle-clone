import React from 'react';

import WordGrid from './WordGrid';
import Keyboard from './Keyboard';

import style from './index.module.scss';

const App = () => {

  return (
    <div className={style.wrap}>

      <div className={style.wrapGame}>

        <header>
          <h1>Wordle</h1>
        </header>

        <WordGrid />

        <Keyboard />

      </div>

    </div>
  );
}

export default App;
