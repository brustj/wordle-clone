import React from 'react';
import { times } from 'lodash';

import style from './index.module.scss';

const WordGrid = ({
  guesses = 6,
  wordLen = 5
}) => {

  return (
    <div className={style.wrap}>

      {times(guesses, (i) =>
        <div
          key={`word-${i}`}
          className={style.wrapWord}>
          {times(wordLen, (j) =>
            <div
              key={`letter-${j}`}
              className={style.letter}>

            </div>
          )}
        </div>
      )}

    </div>
  );
}

export default WordGrid;
